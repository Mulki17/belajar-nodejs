import { EventEmitter } from "events";

// object
const emitter = new EventEmitter();


emitter.addListener("hello", (name) => {
    console.info(`Haiii ${name}`);
})
emitter.addListener("hello2", (name) => {
    console.info(`Haiii ${name}`);
})

emitter.emit("hello", "Novi");
emitter.emit("hello2", "andri");