const buffer = Buffer.from("Muhamad Mulki", "utf8");

console.info(buffer.toString());
console.info(buffer.toString("hex"));
console.info(buffer.toString("base64"));

// encoding ke utf8(normal)
const bufferBase64 = Buffer.from("TXVoYW1hZCBNdWxraQ==", "base64");
console.info(bufferBase64.toString("utf8"));