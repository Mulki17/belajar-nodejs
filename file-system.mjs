import fs from "fs/promises";

// mengecek isi file
const buffer = await fs.readFile("contoh.txt");

console.info(buffer.toString());

// membuat file
await fs.writeFile("amalia.txt", "Hello Amalia");