import fs from "fs";

// buat
const writer = fs.createWriteStream("target.log");

writer.write("mulki\n");
writer.write("sukmana");
writer.end();

// baca
const reader = fs.createReadStream("target.log");
reader.addListener("data", (data) => {
    console.info(data.toString());
});

// buat
const writer2 = fs.createWriteStream("target2.log");

writer2.write("mulki\n");
writer2.write("sukmana");
writer2.end();

// baca
const reader2 = fs.createReadStream("target2.log");
reader2.addListener("data", (data) => {
    console.info(data.toString());
});