import util from "util";

const firstName = "mulki";
const lastName = "sukmana";

console.info(`Hello ${firstName} ${lastName}`);
console.info(util.format("Hello %s %s", firstName, lastName));

// JSON
const person = {
    firstName: "Candra",
    lastName: "Herdiansyah"
};

console.info(`Person : ${JSON.stringify(person)}`);
console.info(util.format("Person : %j", person));